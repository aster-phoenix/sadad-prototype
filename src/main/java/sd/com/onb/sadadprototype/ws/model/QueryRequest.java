package sd.com.onb.sadadprototype.ws.model;

import lombok.Data;

@Data
public class QueryRequest {
    
    private String invoiceNo;
    
}

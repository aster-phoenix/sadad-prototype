package sd.com.onb.sadadprototype.ws.model;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class QueryResponse {
    
    private String payerName;
    private int serviceId;
    private String serviceName;
    private BigDecimal amount;
    private int errorCode;
    private String errorMessage;
    private String describtion;
    private String merchantId;
    private String orderId;

    public QueryResponse() {
    }
    
    public QueryResponse(String payerName, int serviceId, String serviceName, BigDecimal amount, int errorCode, String errorMessage, String describtion, String merchantId, String orderId) {
        this.payerName = payerName;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.amount = amount;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.describtion = describtion;
        this.merchantId = merchantId;
        this.orderId = orderId;
    }
    
    // Builder

    public static class Builder {

        public static QueryResponse sampleResponse() {
            return new QueryResponse("Ghazy Abdallah", 801, "Exam", new BigDecimal(100), 200, "OK", "NIC Sadad", "80", "4");
        }
    }
    
}
